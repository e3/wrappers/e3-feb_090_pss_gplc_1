require_feature("VALIDITY-PV")

############################
#  STATUS BLOCK
############################

set_defaults(ARCHIVE=True, VALIDITY_PV="FEB-090:PSS-PLC-1:GCPU_ConnStat")
set_defaults(add_minor_alarm, add_major_alarm, ALARM_IF=False)

define_status_block()

define_template("NOT_HEALTHY",  PV_ZNAM="Healthy",       PV_ONAM="Error")
define_template("HEALTHY",      PV_ZNAM="Error",         PV_ONAM="Healthy")
define_template("SLOTSTAT",     PV_ZNAM="Error",         PV_ONAM="Healthy")
define_template("CONNSTAT",     PV_ZNAM="Disconnected",  PV_ONAM="Connected")

#
# Cabinet
#
add_minor_alarm("RIO2_CabinetError", "Cabinet Error",  PV_NAME="Cabinet_Stat",  PV_DESC="FALSE when any device in the cabinet has an error",  PV_ONAM="Healthy")
add_digital("RIO2_SelectivityModule",                  PV_NAME="SM_Stat",       PV_DESC="TRUE when a selectivity module has an error",   TEMPLATE="NOT_HEALTHY")
add_digital("RIO2_ScalanceHealthy",                    PV_NAME="SCL_Stat",      PV_DESC="TRUE when the scalance switch is healthy",      TEMPLATE="HEALTHY")
add_digital("RIO2_SurgeArresterHealthy",               PV_NAME="SurgeArrOK",    PV_DESC="TRUE when surge arresster is healthy",          TEMPLATE="HEALTHY")
add_digital("RIO2_PS_24VDCHealthy",                    PV_NAME="PS24vOK",       PV_DESC="TRUE when the power supply 24 VDC is healthy",  TEMPLATE="HEALTHY")
add_digital("RIO2_UPS_24VDCfromBat",                   PV_NAME="UPSBatOK",      PV_DESC="TRUE when the UPS 24 VDC is coming from the battery",      PV_ONAM="From Battery")
add_digital("RIO2_UPS_24VDCfromPS",                    PV_NAME="UPS24vOK",      PV_DESC="TRUE when the UPS 24 VDC is coming from the powe supply",  PV_ONAM="From PS")
add_digital("RIO2_UPSAlarm",                           PV_NAME="UPSAlrm",       PV_DESC="TRUE when the UPS has an alarm",          PV_ONAM="UPS Alarm")
add_digital("RIO2_UPS_RTB",                            PV_NAME="UPSBuffReady",  PV_DESC="TRUE when the UPS is ready to buffer",    PV_ONAM="Ready to Buffer")
add_digital("RIO2_UPS_Bat85",                          PV_NAME="UPSBatt85",     PV_DESC="TRUE when the UPS battery is above 85%",  PV_ONAM="Above 85%")

#
# RIO Slots
#
add_digital("RIO2_IM155Healthy",     PV_NAME="IM_Stat",          PV_DESC="TRUE when the IM155 is healthy",    TEMPLATE="SLOTSTAT")
add_digital("RIO2_IM155Connected",   PV_NAME="IM_ConnStat",      PV_DESC="TRUE when the IM155 is connected",  TEMPLATE="CONNSTAT")
add_digital("RIO2_Slot1Healthy",     PV_NAME="Slot1_Stat",       PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO2_Slot1Connected",   PV_NAME="Slot1_ConnStat",   PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO2_Slot2Healthy",     PV_NAME="Slot2_Stat",       PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO2_Slot2Connected",   PV_NAME="Slot2_ConnStat",   PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO2_Slot3Healthy",     PV_NAME="Slot3_Stat",       PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO2_Slot3Connected",   PV_NAME="Slot3_ConnStat",   PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO2_Slot4Healthy",     PV_NAME="Slot4_Stat",       PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO2_Slot4Connected",   PV_NAME="Slot4_ConnStat",   PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO2_Slot5Healthy",     PV_NAME="Slot5_Stat",       PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO2_Slot5Connected",   PV_NAME="Slot5_ConnStat",   PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO2_Slot6Healthy",     PV_NAME="Slot6_Stat",       PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO2_Slot6Connected",   PV_NAME="Slot6_ConnStat",   PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO2_Slot7Healthy",     PV_NAME="Slot7_Stat",       PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO2_Slot7Connected",   PV_NAME="Slot7_ConnStat",   PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO2_Slot8Healthy",     PV_NAME="Slot8_Stat",       PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO2_Slot8Connected",   PV_NAME="Slot8_ConnStat",   PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO2_Slot9Healthy",     PV_NAME="Slot9_Stat",       PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO2_Slot9Connected",   PV_NAME="Slot9_ConnStat",   PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO2_Slot10Healthy",    PV_NAME="Slot10_Stat",      PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO2_Slot10Connected",  PV_NAME="Slot10_ConnStat",  PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO2_Slot11Healthy",    PV_NAME="Slot11_Stat",      PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO2_Slot11Connected",  PV_NAME="Slot11_ConnStat",  PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO2_Slot12Healthy",    PV_NAME="Slot12_Stat",      PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO2_Slot12Connected",  PV_NAME="Slot12_ConnStat",  PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO2_Slot13Healthy",    PV_NAME="Slot13_Stat",      PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO2_Slot13Connected",  PV_NAME="Slot13_ConnStat",  PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO2_Slot14Healthy",    PV_NAME="Slot14_Stat",      PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO2_Slot14Connected",  PV_NAME="Slot14_ConnStat",  PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO2_Slot15Healthy",    PV_NAME="Slot15_Stat",      PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO2_Slot15Connected",  PV_NAME="Slot15_ConnStat",  PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO2_Slot16Healthy",    PV_NAME="Slot16_Stat",      PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO2_Slot16Connected",  PV_NAME="Slot16_ConnStat",  PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO2_Slot17Healthy",    PV_NAME="Slot17_Stat",      PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO2_Slot17Connected",  PV_NAME="Slot17_ConnStat",  PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO2_Slot18Healthy",    PV_NAME="Slot18_Stat",      PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO2_Slot18Connected",  PV_NAME="Slot18_ConnStat",  PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO2_Slot19Healthy",    PV_NAME="Slot19_Stat",      PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO2_Slot19Connected",  PV_NAME="Slot19_ConnStat",  PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO2_Slot20Healthy",    PV_NAME="Slot20_Stat",      PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO2_Slot20Connected",  PV_NAME="Slot20_ConnStat",  PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO2_Slot21Healthy",    PV_NAME="Slot21_Stat",      PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO2_Slot21Connected",  PV_NAME="Slot21_ConnStat",  PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
