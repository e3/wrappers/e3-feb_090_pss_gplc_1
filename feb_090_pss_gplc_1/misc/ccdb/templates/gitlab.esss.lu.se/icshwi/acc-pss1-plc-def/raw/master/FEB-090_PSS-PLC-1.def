require_feature("VALIDITY-PV")

############################
#  STATUS BLOCK
############################

set_defaults(ARCHIVE=True, VALIDITY_PV="GCPU_ConnStat")
set_defaults(add_minor_alarm, add_major_alarm, ALARM_IF=False)

define_status_block()

define_template("NOT_HEALTHY",  PV_ZNAM="Healthy",       PV_ONAM="Error")
define_template("HEALTHY",      PV_ZNAM="Error",         PV_ONAM="Healthy")
define_template("SLOTSTAT",     PV_ZNAM="Error",         PV_ONAM="Healthy")
define_template("CONNSTAT",     PV_ZNAM="Disconnected",  PV_ONAM="Connected")

add_minor_alarm("StandardHardwareAlarm", "Non-safety HW Error",  PV_NAME="StandardHWAlrm",          PV_DESC="PSS1 has a hardware error not related to safety devices (FALSE = error)")
add_major_alarm("SafetyHardwareAlarm", "Safety HW Error",        PV_NAME="SafetyHWAlrm",            PV_DESC="PSS1 has a hardware error related to safety devices (FALSE = error)")
add_digital("FIOReintegrationRequired",                          PV_NAME="SafetyHW_ReintRequired",  PV_DESC="Reintegration/acknowledgement is required for the safety hardware (TRUE = required)",  PV_ONAM="Reintegration required")
add_analog("StandardChecksum0", "BYTE",                          PV_NAME="StandardSW_Chksum0",      PV_DESC="Checksum for standard program of the safety PLC")
add_analog("StandardChecksum1", "BYTE",                          PV_NAME="StandardSW_Chksum1",      PV_DESC="Checksum for standard program of the safety PLC")
add_analog("StandardChecksum2", "BYTE",                          PV_NAME="StandardSW_Chksum2",      PV_DESC="Checksum for standard program of the safety PLC")
add_analog("StandardChecksum3", "BYTE",                          PV_NAME="StandardSW_Chksum3",      PV_DESC="Checksum for standard program of the safety PLC")
add_analog("StandardChecksum4", "BYTE",                          PV_NAME="StandardSW_Chksum4",      PV_DESC="Checksum for standard program of the safety PLC")
add_analog("StandardChecksum5", "BYTE",                          PV_NAME="StandardSW_Chksum5",      PV_DESC="Checksum for standard program of the safety PLC")
add_analog("StandardChecksum6", "BYTE",                          PV_NAME="StandardSW_Chksum6",      PV_DESC="Checksum for standard program of the safety PLC")
add_analog("StandardChecksum7", "BYTE",                          PV_NAME="StandardSW_Chksum7",      PV_DESC="Checksum for standard program of the safety PLC")
add_string("SafetyPLCFSignature", 4,                             PV_NAME="SafetySW_FSignature",     PV_DESC="Collective F-signature of safety PLC")
add_time("SafetyPLCTime",                                        PV_NAME="Timestamp",               PV_DESC="Time of safety PLC")
add_minor_alarm("SystemError", "System Error",                   PV_NAME="Sys_Stat",                PV_DESC="The system has an error (FALSE = error)")
add_digital("HMIConnected",                                      PV_NAME="HMI_ConnStat",            PV_DESC="HMI is connected (TRUE = connected)",                    TEMPLATE="CONNSTAT")
add_major_alarm("PLCtoGatewayHealthy", "Disconnected",           PV_NAME="GCPU_ConnStat",           PV_DESC="PLC to Gateway connection is healthy (TRUE = healthy)",  PV_ONAM="Connected", VALIDITY_CONDITION=True)
add_digital("SafetyPLCHealthy",                                  PV_NAME="CPU_Stat",                PV_DESC="Status of the safety PLC is healthy (TRUE = healthy)",   TEMPLATE="SLOTSTAT")
add_digital("SafetyPLCConnected",                                PV_NAME="CPU_ConnStat",            PV_DESC="Safety PLC is connected (TRUE = connected)",             TEMPLATE="CONNSTAT")
add_major_alarm("SafetyPLCStartup", "Startup detected",          PV_NAME="CPU_Startup",             PV_DESC="Safety PLC startup detected (TRUE = startup detected and not acknowledged)", ALARM_IF=True)
add_digital("PASHMIConnected",                                   PV_NAME="PASHMI_ConnStat",         PV_DESC="PAS HMI is connected (TRUE = connected)",  TEMPLATE="CONNSTAT")


add_verbatim("""
record(calcout, "[PLCF#INSTALLATION_SLOT]:iDiagErrorCalc")
{
    field(SCAN, "1 second")
    field(INPA, "[PLCF#INSTALLATION_SLOT]:StandardHWAlrm CP MSS")
    field(INPB, "[PLCF#INSTALLATION_SLOT]:SafetyHWAlrm CP MSS")
    field(INPC, "[PLCF#INSTALLATION_SLOT]:Sys_Stat CP MSS")
    field(INPD, "[PLCF#INSTALLATION_SLOT]:GCPU_ConnStat CP MSS")
    field(INPE, "[PLCF#ROOT_INSTALLATION_SLOT]:AliveR")
    field(CALC, "!(A && B && C && D && E)")
    field(OUT,  "[PLCF#INSTALLATION_SLOT]:DiagError PP MSS")
}


record(bi, "[PLCF#INSTALLATION_SLOT]:DiagError")
{
    field(DESC, "TRUE on diagnostics error")
    field(ZNAM, "Nominal")
    field(ONAM, "Error")
}
""")
