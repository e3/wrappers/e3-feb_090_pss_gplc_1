require_feature("VALIDITY-PV")

############################
#  STATUS BLOCK
############################

set_defaults(ARCHIVE=True, VALIDITY_PV="FEB-090:PSS-PLC-1:GCPU_ConnStat")
set_defaults(add_minor_alarm, add_major_alarm, ALARM_IF=False)

define_status_block()

define_template("NOT_HEALTHY",  PV_ZNAM="Healthy",       PV_ONAM="Error")
define_template("HEALTHY",      PV_ZNAM="Error",         PV_ONAM="Healthy")
define_template("SLOTSTAT",     PV_ZNAM="Error",         PV_ONAM="Healthy")
define_template("CONNSTAT",     PV_ZNAM="Disconnected",  PV_ONAM="Connected")

#
# Cabinet
#
add_minor_alarm("RIO5_CabinetError", "Cabinet Error",  PV_NAME="Cabinet_Stat",  PV_DESC="FALSE when any device in the cabinet has an error",  PV_ONAM="Healhty")
add_digital("RIO5_SelectivityModule",                  PV_NAME="SM_Stat",       PV_DESC="TRUE when a selectivity module has an error",  TEMPLATE="NOT_HEALTHY")
add_digital("RIO5_ScalanceHealthy",                    PV_NAME="SCL_Stat",      PV_DESC="TRUE when the scalance switch is healthy",     TEMPLATE="HEALTHY")
add_digital("RIO5_SurgeArresterHealthy",               PV_NAME="SurgeArrOK",    PV_DESC="TRUE when surge arresster is healthy",         TEMPLATE="HEALTHY")
add_digital("RIO5_PS_24VDCHealthy",                    PV_NAME="PS24vOK",       PV_DESC="TRUE when the power supply 24 VDC is healthy", TEMPLATE="HEALTHY")
add_digital("RIO5_UPS_24VDCfromBat",                   PV_NAME="UPSBatOK",      PV_DESC="TRUE when the UPS 24 VDC is coming from the battery",      PV_ONAM="From Battery")
add_digital("RIO5_UPS_24VDCfromPS",                    PV_NAME="UPS24vOK",      PV_DESC="TRUE when the UPS 24 VDC is coming from the powe supply",  PV_ONAM="From PS")
add_digital("RIO5_UPSAlarm",                           PV_NAME="UPSAlrm",       PV_DESC="TRUE when the UPS has an alarm",                           PV_ONAM="UPS Alarm")
add_digital("RIO5_UPS_RTB",                            PV_NAME="UPSBuffReady",  PV_DESC="TRUE when the UPS is ready to buffer",                     PV_ONAM="Ready to Buffer")
add_digital("RIO5_UPS_Bat85",                          PV_NAME="UPSBatt85",     PV_DESC="TRUE when the UPS battery is above 85%",                   PV_ONAM="Above 85%")
add_digital("RIO5_RCBO_FB1",                           PV_NAME="RCBO_FB1_OK",   PV_DESC="TRUE when Residual Current Circuit Breaker protection device 1 is healthy",  TEMPLATE="HEALTHY")
add_digital("RIO5_RCBO_FB2",                           PV_NAME="RCBO_FB2_OK",   PV_DESC="TRUE when Residual Current Circuit Breaker protection device 2 is healthy",  TEMPLATE="HEALTHY")

#
# RIO
#
add_digital("RIO5_IM155Healthy",     PV_NAME="IM_Stat",          PV_DESC="TRUE when the IM155 is healthy",    TEMPLATE="SLOTSTAT")
add_digital("RIO5_IM155Connected",   PV_NAME="IM_ConnStat",      PV_DESC="TRUE when the IM155 is connected",  TEMPLATE="CONNSTAT")
add_digital("RIO5_Slot1Healthy",     PV_NAME="Slot1_Stat",       PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO5_Slot1Connected",   PV_NAME="Slot1_ConnStat",   PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO5_Slot2Healthy",     PV_NAME="Slot2_Stat",       PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO5_Slot2Connected",   PV_NAME="Slot2_ConnStat",   PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO5_Slot3Healthy",     PV_NAME="Slot3_Stat",       PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO5_Slot3Connected",   PV_NAME="Slot3_ConnStat",   PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO5_Slot4Healthy",     PV_NAME="Slot4_Stat",       PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO5_Slot4Connected",   PV_NAME="Slot4_ConnStat",   PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO5_Slot5Healthy",     PV_NAME="Slot5_Stat",       PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO5_Slot5Connected",   PV_NAME="Slot5_ConnStat",   PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO5_Slot6Healthy",     PV_NAME="Slot6_Stat",       PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO5_Slot6Connected",   PV_NAME="Slot6_ConnStat",   PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO5_Slot7Healthy",     PV_NAME="Slot7_Stat",       PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO5_Slot7Connected",   PV_NAME="Slot7_ConnStat",   PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO5_Slot8Healthy",     PV_NAME="Slot8_Stat",       PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO5_Slot8Connected",   PV_NAME="Slot8_ConnStat",   PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO5_Slot9Healthy",     PV_NAME="Slot9_Stat",       PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO5_Slot9Connected",   PV_NAME="Slot9_ConnStat",   PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO5_Slot10Healthy",    PV_NAME="Slot10_Stat",      PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO5_Slot10Connected",  PV_NAME="Slot10_ConnStat",  PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO5_Slot11Healthy",    PV_NAME="Slot11_Stat",      PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO5_Slot11Connected",  PV_NAME="Slot11_ConnStat",  PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO5_Slot12Healthy",    PV_NAME="Slot12_Stat",      PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO5_Slot12Connected",  PV_NAME="Slot12_ConnStat",  PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO5_Slot13Healthy",    PV_NAME="Slot13_Stat",      PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO5_Slot13Connected",  PV_NAME="Slot13_ConnStat",  PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO5_Slot14Healthy",    PV_NAME="Slot14_Stat",      PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO5_Slot14Connected",  PV_NAME="Slot14_ConnStat",  PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO5_Slot15Healthy",    PV_NAME="Slot15_Stat",      PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO5_Slot15Connected",  PV_NAME="Slot15_ConnStat",  PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO5_Slot16Healthy",    PV_NAME="Slot16_Stat",      PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO5_Slot16Connected",  PV_NAME="Slot16_ConnStat",  PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO5_Slot17Healthy",    PV_NAME="Slot17_Stat",      PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO5_Slot17Connected",  PV_NAME="Slot17_ConnStat",  PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO5_Slot18Healthy",    PV_NAME="Slot18_Stat",      PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO5_Slot18Connected",  PV_NAME="Slot18_ConnStat",  PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO5_Slot19Healthy",    PV_NAME="Slot19_Stat",      PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO5_Slot19Connected",  PV_NAME="Slot19_ConnStat",  PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
add_digital("RIO5_Slot20Healthy",    PV_NAME="Slot20_Stat",      PV_DESC="TRUE when the slot is healthy",     TEMPLATE="SLOTSTAT")
add_digital("RIO5_Slot20Connected",  PV_NAME="Slot20_ConnStat",  PV_DESC="TRUE when the slot is connected",   TEMPLATE="CONNSTAT")
